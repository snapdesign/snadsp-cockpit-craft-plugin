# DSP Cockpit plugin for Craft CMS 3.x

DSP Management Cockpit Plugin

![Screenshot](resources/img/plugin-logo.png)

## Requirements

This plugin requires Craft CMS 3.0.0-beta.23 or later.

## Installation

To install the plugin, follow these instructions.

1. Open your terminal and go to your Craft project:

        cd /path/to/project

2. Then tell Composer to load the plugin:

        composer require /dsp-cockpit

3. In the Control Panel, go to Settings → Plugins and click the “Install” button for DSP Cockpit.

## DSP Cockpit Overview

-Insert text here-

## Configuring DSP Cockpit

-Insert text here-

## Using DSP Cockpit

-Insert text here-

## DSP Cockpit Roadmap

Some things to do, and ideas for potential features:

* Release it

Brought to you by [Snapdesign AG](https://snapdesign.ch)
