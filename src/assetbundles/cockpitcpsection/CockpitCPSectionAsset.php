<?php
/**
 * DSP Cockpit plugin for Craft CMS 3.x
 *
 * DSP Management Cockpit Plugin
 *
 * @link      https://snapdesign.ch
 * @copyright Copyright (c) 2018 Snapdesign AG
 */

namespace snapdesign\dspcockpit\assetbundles\cockpitcpsection;

use Craft;
use craft\web\AssetBundle;
use craft\web\assets\cp\CpAsset;

/**
 * @author    Snapdesign AG
 * @package   DspCockpit
 * @since     1.0.0
 */
class CockpitCPSectionAsset extends AssetBundle
{
    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->sourcePath = "@snapdesign/dspcockpit/assetbundles/cockpitcpsection/dist";

        $this->depends = [
            CpAsset::class,
        ];

        $this->js = [
            'js/Cockpit.js',
        ];

        $this->css = [
            'css/Cockpit.css',
        ];

        parent::init();
    }
}
