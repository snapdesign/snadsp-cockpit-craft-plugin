<?php
/**
 * DSP Cockpit plugin for Craft CMS 3.x
 *
 * DSP Management Cockpit Plugin
 *
 * @link      https://snapdesign.ch
 * @copyright Copyright (c) 2018 Snapdesign AG
 */

namespace snapdesign\dspcockpit;


use Craft;
use craft\base\Plugin;
use craft\services\Plugins;
use craft\events\PluginEvent;

use yii\base\Event;

/**
 * Class DspCockpit
 *
 * @author    Snapdesign AG
 * @package   DspCockpit
 * @since     1.0.0
 *
 */
class DspCockpit extends Plugin
{
    // Static Properties
    // =========================================================================

    /**
     * @var DspCockpit
     */
    public static $plugin;

    // Public Properties
    // =========================================================================

    /**
     * @var string
     */
    public $schemaVersion = '1.0.0';

    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        self::$plugin = $this;

        Event::on(
            Plugins::class,
            Plugins::EVENT_AFTER_INSTALL_PLUGIN,
            function (PluginEvent $event) {
                if ($event->plugin === $this) {
                }
            }
        );

        Craft::info(
            Craft::t(
                'dsp-cockpit',
                '{name} plugin loaded',
                ['name' => $this->name]
            ),
            __METHOD__
        );
    }

    // Protected Methods
    // =========================================================================

}
