<?php
/**
 * DSP Cockpit plugin for Craft CMS 3.x
 *
 * DSP Management Cockpit Plugin
 *
 * @link      https://snapdesign.ch
 * @copyright Copyright (c) 2018 Snapdesign AG
 */

/**
 * @author    Snapdesign AG
 * @package   DspCockpit
 * @since     1.0.0
 */
return [
    'DSP Cockpit plugin loaded' => 'DSP Cockpit plugin loaded',
];
